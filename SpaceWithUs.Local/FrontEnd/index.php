<!DOCTYPE html>
<html lang="en">
<?php include 'partials/head.php'; ?>

<body class="space-body" id="main-content">

	<?php include 'partials/header.php'; ?>

	<main class="space-main">

		<?php include 'templates/flexible-content.php'; ?>

	</main>

	<?php include 'partials/footer.php'; ?>
	<?php include 'partials/js-enqueue.php'; ?>
</body>

</html>