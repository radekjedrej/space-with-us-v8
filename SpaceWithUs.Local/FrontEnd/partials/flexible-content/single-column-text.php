<div class="single-column-text__wrapper">
    <div class="single-column-text">
        <h3 style="text-align: center;">Single column Intro text</h3>
        <p style="text-align: center;">The Attenborough Centre for the Creative Arts (ACCA) is the newest addition to the conference offer at the University. A large theatre with outstanding acoustics and facilities second to none. The Attenborough Centre stands out among events venues in Brighton for its rich heritage, unique design and connections to the University.</p>
        <p style="text-align: center;">The Attenborough Centre for the Creative Arts (ACCA) is the newest addition to the conference offer at the University. A large theatre with outstanding acoustics and facilities second to none. The Attenborough Centre stands out among events venues in Brighton for its rich heritage, unique design and connections to the University.</p>
    </div>
</div>