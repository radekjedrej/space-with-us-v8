<div class="two-image__wrapper">
    <div class="section-container">
        <div class="two-image__container">
            <div class="two-image__main-title">
                <h2 class="heading-2-1">Training
                <!-- TODO: Let user to choose a underline colour "bg-yellow, bg-green, bg-orange, bg-pink, bg-navy, $bg-blue" -->
                <div class="two-image__main-title__underline bg-navy"></div></h2>
            </div>
            <div class="two-image">
                <div class="two-image__inner two-image__inner--up">
                    <div class="two-image__box two-image__box__bg lazy" style="background-image:url('https://picsum.photos/404')"></div>
                    <div class="two-image__box">
                        <div class="two-image__box__img"></div>
                        <div class="two-image__box__copy two-image__box__copy--top">
                            <h3 class="two-image__box__title">Space for events</h3>
                            <div class="two-image__box__text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div class="two-image__box__text--sm heading-6">
                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                            <div class="two-image__btn btn__wrapper">
                                <a class="space-btn" href="#">Learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="two-image__inner two-image__inner--down">
                    <div class="two-image__box">
                        <div class="two-image__box__img"></div>
                        <div class="two-image__box__copy two-image__box__copy--bottom">
                            <h3 class="two-image__box__title">Space for events</h3>
                            <div class="two-image__box__text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div class="two-image__box__text--sm heading-6">
                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                            <div class="two-image__btn btn__wrapper">
                                <a class="space-btn" href="#">Learn more</a>
                            </div>
                        </div>
                    </div>
                    <div class="two-image__box two-image__box__bg lazy" style="background-image:url('https://picsum.photos/404')"></div>
                </div>
            </div>
        </div>
    </div>
</div>