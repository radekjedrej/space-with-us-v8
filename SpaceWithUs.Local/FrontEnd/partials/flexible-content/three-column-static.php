<div class="three-column-static__wrapper">
    <div class="section-container">
        <div class="js-three-column-static three-column-static">
            <div href="#" class="three-column-static__box">
                <div class="three-column-static__inner">
                    <div class="three-column-static__img">
                        <img class="lazy" data-src="https://picsum.photos/448/360" alt="">
                    </div>
                    <div class="three-column-static__copy">
                        <div class="three-column-static__copy--top">
                            <h3 class="three-column-static__title">Title text here
                                <div class="three-column-static-title__underline bg-yellow"></div>
                            </h3>
                            <p class="three-column-static__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div href="#" class="three-column-static__box">
                <div class="three-column-static__inner">
                    <div class="three-column-static__img">
                        <img class="lazy" data-src="https://picsum.photos/448/360" alt="">
                    </div>
                    <div class="three-column-static__copy">
                        <div class="three-column-static__copy--top">
                            <h3 class="three-column-static__title">Title text here
                                <div class="three-column-static-title__underline bg-yellow"></div>
                            </h3>
                            <p class="three-column-static__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div href="#" class="three-column-static__box">
                <div class="three-column-static__inner">
                    <div class="three-column-static__img">
                        <img class="lazy" data-src="https://picsum.photos/448/360" alt="">
                    </div>
                    <div class="three-column-static__copy">
                     <div class="three-column-static__copy--top">
                            <h3 class="three-column-static__title">Title text here
                                <div class="three-column-static-title__underline bg-yellow"></div>
                            </h3>
                            <p class="three-column-static__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>