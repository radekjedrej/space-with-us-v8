<div class="accordion__wrapper bg-primary">
    <div class="section-container">
        <div class="accordion">
            <div class="accordion__title">
                <h2>Available Spaces
                    <div class="accordion-title__underline bg-yellow"></div>
                </h2>
            </div>
            <div class="accordion__box__wrapper">
                <div class="accordion__box">
                    <h3 class="js-accordion-toggle accordion__box__title heading-3" href="#">Space 1</h3>
                    <ul class="accordion__ul accordion__inner">
                        <!-- TODO: Let user create custom text -->
                        <li class="accordion__li accordion__li--inner">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum perferendis architecto temporibus quidem quasi debitis ipsum, officia repellendus omnis in qui non neque hic a id laudantium deserunt quae voluptatibus harum dolore similique odio quod. Dolorem quos, at deserunt fugiat dignissimos sed qui maiores repellendus quaerat eos. Vitae beatae quae numquam aperiam consequatur dicta esse, quos animi facilis quod praesentium id culpa, architecto fuga facere omnis. Hic rem commodi quam, minus eaque animi voluptas cupiditate cum expedita velit, placeat ipsam?</li>
                    </ul>
                </div>

                <div class="accordion__box">
                    <h3 class="js-accordion-toggle accordion__box__title heading-3" href="#">LongTitle textname here</h3>
                    <ul class="accordion__ul accordion__inner">
                        <!-- TODO: Let user create custom text -->
                        <li class="accordion__li accordion__li--inner">
                            <h3>Title name here</h3>
                            <p>‘It’s a Knock Out’, orienteering or summer BBQ food parties.Book a unique outdoor space and take advantage of breathtaking views of the South Downs National Park. We offer football, cricket and rugby pitches to hire as well as acres of open space for bespoke events such as ‘It’s a Knock Out’, orienteering or summer BBQ food parties. Cricket and rugby pitches to hire as well as acres of open space for bespoke events such as ‘It’s a Knock Out’, orienteering or summer BBQ food parties.</p>
                            <p>‘It’s a Knock Out’, orienteering or summer BBQ food parties.Book a unique outdoor space and take advantage of breathtaking views of the South Downs National Park. We offer football, cricket and rugby pitches to hire as well as acres of open space for bespoke events such as ‘It’s a Knock Out’, orienteering or summer BBQ food parties. Cricket and rugby pitches to hire as well as acres of open space for bespoke events such as ‘It’s a Knock Out’, orienteering or summer BBQ food parties.</p>
                        </li>
                    </ul>
                </div>

                <div class="accordion__box">
                    <h3 class="js-accordion-toggle accordion__box__title heading-3" href="#">LongTitle textname here</h3>
                    <ul class="accordion__ul accordion__inner">
                        <!-- TODO: Let user create custom text -->
                        <li class="accordion__li accordion__li--inner">
                            <h3>Title name here</h3>
                            <p>‘It’s a Knock Out’, orienteering or summer BBQ food parties.Book a unique outdoor space and take advantage of breathtaking views of the South Downs National Park. We offer football, cricket and rugby pitches to hire as well as acres of open space for bespoke events such as ‘It’s a Knock Out’, orienteering or summer BBQ food parties. Cricket and rugby pitches to hire as well as acres of open space for bespoke events such as ‘It’s a Knock Out’, orienteering or summer BBQ food parties.</p>
                            <p>‘It’s a Knock Out’, orienteering or summer BBQ food parties.Book a unique outdoor space and take advantage of breathtaking views of the South Downs National Park. We offer football, cricket and rugby pitches to hire as well as acres of open space for bespoke events such as ‘It’s a Knock Out’, orienteering or summer BBQ food parties. Cricket and rugby pitches to hire as well as acres of open space for bespoke events such as ‘It’s a Knock Out’, orienteering or summer BBQ food parties.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>