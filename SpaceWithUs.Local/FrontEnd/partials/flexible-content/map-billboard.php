<div class="map-billboard">
  <!-- TODO: Let user to add data-lat, data-lng and data-direction -->
  <div class="acf-map js-event-map map-billboard__map">
    <div class="marker js-marker" data-lat="50.8663864" data-lng="-0.0870514">
      <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Autem iusto, optio alias, atque dolor doloribus ipsum commodi, suscipit consequatur doloremque est adipisci delectus soluta repellat amet iure ipsam repellendus voluptatibus.</p>
      <a href="https://www.google.com/maps/dir//University+of+Sussex,+Falmer,+Brighton+BN1+9RH/@50.8663864,-0.0870514,15.57z/data=!4m9!4m8!1m0!1m5!1m1!1s0x487588ca9b7c1b1b:0x7b4ba124cd6ad273!2m2!1d-0.087914!2d50.8670895!3e0">Campus 1</a>
    </div>
    <div class="marker js-marker" data-lat="50.8673864" data-lng="-0.0871514">
    <a href="https://www.google.com/maps/dir//University+of+Sussex,+Falmer,+Brighton+BN1+9RH/@50.8663864,-0.0870514,15.57z/data=!4m9!4m8!1m0!1m5!1m1!1s0x487588ca9b7c1b1b:0x7b4ba124cd6ad273!2m2!1d-0.087914!2d50.8670895!3e0">Campus 2</a>
    </div>
    <div class="marker js-marker" data-lat="50.8783864" data-lng="-0.0872514">
    <a href="https://www.google.com/maps/dir//University+of+Sussex,+Falmer,+Brighton+BN1+9RH/@50.8663864,-0.0870514,15.57z/data=!4m9!4m8!1m0!1m5!1m1!1s0x487588ca9b7c1b1b:0x7b4ba124cd6ad273!2m2!1d-0.087914!2d50.8670895!3e0">Campus 2</a>
    </div>
  </div>
</div>