<div class="price-table__wrapper">
    <div class="section-container">
        <div class="price-table">
            <h3 class="price-table__heading">Our Spaces Pricing</h3>
            <div class="price-table__inner">

                <table class="price-table__table">
                    <tbody>
                        <tr>
                            <td class="price-table__place">ACCA</td>
                            <td class="price-table__price">£345.00</td>
                        </tr>
                        <tr>
                            <td class="price-table__place">Fulton</td>
                            <td class="price-table__price">£1345.00</td>
                        </tr>
                        <tr>
                            <td class="price-table__place">Jubilee</td>
                            <td class="price-table__price">£11345.00</td>
                        </tr>
                        <tr>
                            <td class="price-table__place">Conference Centre</td>
                            <td class="price-table__price">£45.00</td>
                        </tr>
                    </tbody>
                </table>

                <div class="price-table__text">
                    <div class="price-table__content">
                        <p>‘It’s a Knock Out’, orienteering or summer BBQ food parties.Book a unique outdoor space and take advantage of breathtaking views of the South Downs National Park. We offer football, cricket and rugby pitches to hire as well as acres of open space for bespoke events such as ‘It’s a Knock Out’, orienteering or summer BBQ food parties. Cricket and rugby pitches to hire as well as acres of open space for bespoke events such as ‘It’s a Knock Out’, orienteering or summer BBQ food parties.</p>
                    </div>
                    <div class="price-table__btn btn__wrapper">
                        <a class="space-btn" href="#">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>