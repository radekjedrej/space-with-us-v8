<div class="contact-banner__wrapper">

    <div class="contact-banner__bottom bg-primary">
        <div class="section-container">
            <div class="contact-banner__bottom__text">
                <div class="contact-banner__top__title">
                    <h1 class="contact-banner__top__heading">Space to</h1>
                    <h1 class="contact-banner__top__heading contact-banner__top__heading--bottom">Contact us</h1>
                    <div class="contact-banner__top__underline"></div>
                </div>
                <div class="contact-banner__info">
                    <div class="contact-banner__bottom__subtitle">
                        <p>During vacation periods we have access to an abundance of stylish state of the art lecture theatres, meeting spaces and exhibition areas, all located a short stroll from our dining and accommodation hubs.</p>
                    </div>
                    <div class="contact-banner__pricing">
                        <table class="contact-banner__table">
                            <tbody>
                                <tr>
                                    <td class="contact-banner__icon"><img src="../assets/img/icons/telephone.svg" alt=""></td>
                                    <td class="contact-banner__price">+44 (0)1273 678678</td>
                                </tr>
                                <tr>
                                    <td class="contact-banner__icon"><img src="../assets/img/icons/business.svg" alt=""></td>
                                    <td class="contact-banner__price">spacewithus@sussex.ac.uk</td>
                                </tr>
                                <tr>
                                    <td class="contact-banner__icon"><img src="../assets/img/icons/maps-and-flags.svg" alt=""></td>
                                    <td class="contact-banner__price">University of Sussex, Level 4 Bramber House Brighton, East Sussex, BN1 9QU</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-banner__bottom__bg">
            <div class="contact-banner__carousel">
                <div class="contact-banner__slide" style="background-image:url('https://picsum.photos/800/300')"></div>
            </div>
        </div>
    </div>

    <a href="#" class="contact-banner__chat"></a>
</div>