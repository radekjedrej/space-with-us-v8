<div class="three-column-link__wrapper">
    <div class="section-container">
        <div class="js-three-column-link three-column-link">
            <a href="#" class="three-column-link__box">
                <div class="three-column-link__inner bg-green">
                    <div class="three-column-link__img">
                        <img class="lazy" data-src="https://picsum.photos/448/360" alt="">
                    </div>
                    <div class="three-column-link__copy">
                        <div class="three-column-link__copy--top">
                            <h3 class="three-column-link__title">Title text here</h3>
                            <p class="three-column-link__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div class="three-column-link__icon">
                            <svg fill="#FFF" width="42" height="42" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39 28"><path d="M21.75 20.5l10-6.5-10-6.5zm2-9.31L28.08 14l-4.33 2.81z"/><path d="M24.75 0a14 14 0 00-14 13.5H.25v2h10.58A14 14 0 1024.75 0zm0 26a12 12 0 01-11.9-10.5h4.4v-2h-4.47a12 12 0 1112 12.5z"/></svg>
                        </div>
                    </div>
                </div>
            </a>

            <a href="#" class="three-column-link__box">
                <div class="three-column-link__inner bg-pink">
                    <div class="three-column-link__img">
                        <img class="lazy" data-src="https://picsum.photos/448/360" alt="">
                    </div>
                    <div class="three-column-link__copy">
                        <div class="three-column-link__copy--top">
                            <h3 class="three-column-link__title">Title text here</h3>
                            <p class="three-column-link__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                        <div class="three-column-link__icon">
                            <svg fill="#FFF" width="42" height="42" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39 28"><path d="M21.75 20.5l10-6.5-10-6.5zm2-9.31L28.08 14l-4.33 2.81z"/><path d="M24.75 0a14 14 0 00-14 13.5H.25v2h10.58A14 14 0 1024.75 0zm0 26a12 12 0 01-11.9-10.5h4.4v-2h-4.47a12 12 0 1112 12.5z"/></svg>
                        </div>
                    </div>
                </div>
            </a>

            <a href="#" class="three-column-link__box">
                <div class="three-column-link__inner bg-orange">
                    <div class="three-column-link__img">
                        <img class="lazy" data-src="https://picsum.photos/448/360" alt="">
                    </div>
                    <div class="three-column-link__copy">
                     <div class="three-column-link__copy--top">
                            <h3 class="three-column-link__title">Title text here</h3>
                            <p class="three-column-link__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div class="three-column-link__icon">
                            <svg fill="#FFF" width="42" height="42" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39 28"><path d="M21.75 20.5l10-6.5-10-6.5zm2-9.31L28.08 14l-4.33 2.81z"/><path d="M24.75 0a14 14 0 00-14 13.5H.25v2h10.58A14 14 0 1024.75 0zm0 26a12 12 0 01-11.9-10.5h4.4v-2h-4.47a12 12 0 1112 12.5z"/></svg>
                        </div>
                    </div>
                </div>
            </a>
            
        </div>
    </div>
</div>