<div class="secondary-billboard__wrapper">

    <div class="secondary-billboard__bottom bg-green">
        <div class="section-container">
            <div class="secondary-billboard__bottom__text">
                <div class="secondary-billboard__top__title">
                    <h1 class="secondary-billboard__top__heading">Use our</h1>
                    <h1 class="secondary-billboard__top__heading secondary-billboard__top__heading--bottom">Space</h1>
                    <div class="secondary-billboard__top__underline"></div>
                </div>
                <div class="secondary-billboard__bottom__copy">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <div class="secondary-billboard__btn btn__wrapper">
                        <a class="space-btn" href="#">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="secondary-billboard__bottom__bg">
            <div class="js-secondary-billboard secondary-billboard__carousel">
                <div class="secondary-billboard__slide" style="background-image:url('https://picsum.photos/800/300')"></div>
                <div class="secondary-billboard__slide" style="background-image:url('https://picsum.photos/800/300')"></div>
                <div class="secondary-billboard__slide" style="background-image:url('https://picsum.photos/800/300')"></div>
            </div>
            <div class="secondary-billboard__nav previous-caro-secondary"><span class="nav-previous-secondary"></span></div>
            <div class="secondary-billboard__nav next-caro-secondary"><span class="nav-next-secondary"></span></div>
        </div>
    </div>

    <a href="#" class="secondary-billboard__chat"></a>
</div>