<div class="home-billboard__wrapper">

    <div class="home-billboard__top lazy" style="background-image:url('https://picsum.photos/1600/800')">
        <div class="home-billboard__overlay"></div>
        <div class="section-container">
            <div class="home-billboard__top__text">
                <div class="home-billboard__top__text__inner">
                    <div class="home-billboard__top__title">
                        <h1 class="home-billboard__top__heading">Use our</h1>
                        <h1 class="home-billboard__top__heading home-billboard__top__heading--bottom">Space</h1>
                    </div>
                    <p class="home-billboard__top__copy">Whether you’re organising corporate team building, sporting competitions, summer camps or a private party, our spaces offer excellent value and convenience.</p>
                    <div class="home-billboard__btn btn__wrapper">
                        <a class="space-btn" href="#">Learn more</a>
                    </div>

                    <div class="home-billboard__top__social">
                        <a href="#" class="home-billboard__top__instagram"></a>
                        <a href="#" class="home-billboard__top__facebook"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="home-billboard__bottom bg-green">
        <div class="section-container">
            <div class="home-billboard__bottom__text">
                <h3 class="home-billboard__bottom__heading">Events</h3>
                <p class="home-billboard__bottom__copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </div>
        </div>
        <div class="home-billboard__bottom__bg">
            <div class="js-home-billboard home-billboard__carousel">
                <div class="home-billboard__slide" style="background-image:url('https://picsum.photos/800/300')"></div>
                <div class="home-billboard__slide" style="background-image:url('https://picsum.photos/800/300')"></div>
                <div class="home-billboard__slide" style="background-image:url('https://picsum.photos/800/300')"></div>
            </div>
            <div class="home-billboard__nav previous-caro"><span class="nav-previous"></span></div>
            <div class="home-billboard__nav next-caro"><span class="nav-next"></span></div>
        </div>
    </div>

    <a href="#" class="home-billboard__chat"></a>
</div>